import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {
  numOfClicks=0;
  showSecret=false;
  secret="Secret Password = tuna";
  arrayClicks=[];
  constructor() { }

  ngOnInit() {
  }
  toggleSecret(){
    this.showSecret=!this.showSecret;
    this.numOfClicks++;
    this.arrayClicks.push(this.numOfClicks);
  }
  getIndex(clicks){
    return clicks >= 5 ? 'blue' : 'white';
  }

}
